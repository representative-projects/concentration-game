new Vue({
    el: '#game',
    data: {
        event: {},
        moves: 0,
        guessedCard: '',
        cards: [
            {
                img: 'assets/img/cards/apple.jpg',
                alt: 'apple'
            },
            {
                img: 'assets/img/cards/avocado.jpg',
                alt: 'avocado'
            },
            {
                img: 'assets/img/cards/banana.jpg',
                alt: 'banana'
            },
            {
                img: 'assets/img/cards/carrot.jpg',
                alt: 'carrot'
            },
            {
                img: 'assets/img/cards/grape.jpg',
                alt: 'grape'
            },
            {
                img: 'assets/img/cards/kiwi.jpg',
                alt: 'kiwi'
            },
            {
                img: 'assets/img/cards/mango.jpg',
                alt: 'mango'
            },
            {
                img: 'assets/img/cards/pineapple.jpg',
                alt: 'pineapple'
            },
        ],
        cardsInGame: [],
        cardsOnTable: [],
        timeout: 1000,
    },
    computed: {
        cardsOnPage: function() {
            if (this.cardsInGame.length === 0) {
                this.cardsInGame = this.cards;
            }
            this.cardsOnTable = this.cardsInGame.filter(function(item, index) {
                return 2 > index;
            });
            if (this.cardsOnTable.length === 1) {
                this.cardsOnTable.push(this.cardsOnTable[0]);
            } else {
                for (i = 0; i < 2; i++) {
                    this.cardsOnTable.push(this.cardsOnTable[i]);
                }
            }
            this.shuffleCards();
            return this.cardsOnTable;
        }
    },
    methods: {
        startGame: function(event) {
            this.resetToDefaults();
            $('#game-result').modal('hide');
            $('.button__start').addClass('hidden');
            $('.game__grid').removeClass('hidden');
        },
        shuffleCards: function() {
            this.cardsOnTable = this.cardsOnTable.sort(() => Math.random() - 0.5);
        },
        flipCard: function(event) {
            this.event = event;
            if ($(this.event.currentTarget).hasClass('active') === false) {
                $(this.event.currentTarget)
                    .addClass('active')
                    .find('.game__item__image-top')
                    .addClass('hidden')
                    .parent()
                    .find('.game__item__image-inner')
                    .removeClass('hidden');
                this.checkFlipedCard();
                this.addStep();
            };
        },
        checkFlipedCard: function() {
            if ($(this.event.currentTarget).parent().parent().find('.active').length === 2) {
                this.compareCards()
            }
        },
        compareCards: function() {
            let that = this,
                activeElements = $(this.event.currentTarget).parent().parent().find('.active'),
                firstElementValue = activeElements.eq(0).find('.game__item__image-inner').attr('alt'),
                secondElementValue = activeElements.eq(1).find('.game__item__image-inner').attr('alt');
            if (firstElementValue === secondElementValue) {
                this.guessedCard = firstElementValue;
                setTimeout( function () {
                    that.takeOutThePair();
                    that.shuffleCards();
                }, that.timeout );
            }
            this.newTry();
        },
        takeOutThePair: function() {
            let card = this.guessedCard;
            this.cardsInGame = this.cardsInGame.filter(function(item) {
                return item.alt != card;
            });
            if (this.cardsInGame.length === 0) {
                this.congratulation();
            }
        },
        addStep: function() {
            this.moves++;
        },
        newTry: function() {
            let active = $(this.event.currentTarget).parent().parent().find('.active'),
                timeout = this.timeout;
            active.each(function(index, element) {
                setTimeout(function() {
                    $(element)
                        .removeClass('active')
                        .find('.game__item__image-inner')
                        .addClass('hidden')
                        .parent()
                        .find('.game__item__image-top')
                        .removeClass('hidden');
                }, timeout);
            });
        },
        congratulation: function() {
            $('#game-result').modal('show');
            $('.game__grid').addClass('hidden');
        },
        leave: function() {
            $('.button__start').removeClass('hidden');
        },
        resetToDefaults: function() {
            this.moves = 0;
            this.cardsInGame = this.cards;
            this.shuffleCards();
        }
    }
});